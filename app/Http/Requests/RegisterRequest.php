<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|alpha|min:2|max:60',
            'last_name' => 'required|alpha|min:2|max:60',
            'email' => 'required|email|unique:users',
            'password' => [
                'required',
//                'confirmed',
//                'min:6',
                "regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/"
            ]
        ];
    }

    public function messages()
    {
        return [
//            "required" => ":attribute is a required field"
            "regex" => "Password must be at least 6 characters long. Only letters and digits are allowed and it should have at least one lowercase letter, one uppercase letter and one numeric digit."
        ];
    }
}
