<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email",
            "password" => [ "required"
//                ,"regex:/^[A-z\d]+$/"
                ,"regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/"
            ]
        ];
    }

    public function messages(){
        return [
//            "required" => ":attribute is a required field"
            "regex" => "Password must be at least 6 characters long. Only letters and digits are allowed and it should have at least one lowercase letter, one uppercase letter and one numeric digit."
        ];
    }
}
