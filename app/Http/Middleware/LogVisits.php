<?php

namespace App\Http\Middleware;

use App\Models\LogVisit;
use Closure;
use Illuminate\Http\Request;


class LogVisits
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        LogVisit::log($request->path(), $request->ip());

        return $next($request);
    }
}
