<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\LogActivity;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{


    public function login(LoginRequest $request)
    {
        try {

            $user = User::where('email', $request->email)->first();

            if ($user) {
                if (Hash::check($request->password, $user->password)) {

                    unset($user->password);

                    LogActivity::log($user->id, 'Logged in');

                    $request->session()->put("user", $user);

                    if ($user->role === 'admin') {

                        return redirect()->route('admin.index')->with('msgSuccess', 'Welcome back ' . $user->first_name . '. You can manage website content here.');

                    } else {

                        return redirect()->route('products.index')->with('msgSuccess', 'Welcome ' . $user->first_name . '. You can now make purchases.');

                    }
                } else {

                    return redirect()->back()->with('msgError', 'Password incorrect, please try again');

                }

            } else {

                return redirect()->back()->with('msgError', 'Email address not found, please try again');

            }

        } catch (Exception $e) {
            Log::error($e->getMessage());

            return redirect()->back()->with('msgError', 'An error occurred, please try again');

        }


    }

    public function logout()
    {

        LogActivity::log(session()->get('user')->id, 'Logged out');

        session()->remove("user");
        session()->forget('user');

        return redirect()->route("form.login")->with('msgSuccess', 'You have successfully logged out');

    }

    public function register(RegisterRequest $request)
    {
        $request['password'] = Hash::make($request['password']);

        try {
            $user = User::create($request->toArray());
            LogActivity::log($user->id, 'Registered');

            return redirect()->route("form.login")->with('msgSuccess', 'You have successfully created an account. Now You can Log in ');;

        } catch (Exception $e) {
            Log::error($e->getMessage());

            return redirect()->back()->with('msgError', 'An error occurred, pleas try again');

        }


    }
}
