<?php

namespace App\Http\Controllers;

use App\Models\OrderDetail;
use App\Models\OrderHeader;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CartController extends FrontController
{

    public function add(Request $request)
    {
        $request->validate(['quantity' => 'required|integer|min:1|max:20', 'product_id' => 'required|exists:products,id'], $request->all());

        $productId = $request->product_id;
        $currentPrice = Product::getCurrentPrice($productId);

        $userId = session()->get('user')->id;
        $isPending = OrderHeader::getUnProcessed($userId);

        DB::beginTransaction();

        try {
            if (!$isPending) {

                $header = OrderHeader::addNewOne($userId);
                OrderDetail::addNewOne($header->id, $productId, $request->quantity, $currentPrice);

            } else {
                if (OrderHeader::hasProcessingTimeElapsed($isPending)) {

                    OrderHeader::processOne($isPending);
                    DB::commit();

                    return $this->add($request);

                } else {

                    $containsProduct = OrderDetail::doesOrderAlreadyHaveProduct($isPending->id, $productId);
                    if ($containsProduct) {

                        $newQuantity = $containsProduct->quantity + intval($request->quantity);
                        OrderDetail::changeProductQuantity($containsProduct, $newQuantity);

                    } else {

                        OrderDetail::addNewOne($isPending->id, $productId, $request->quantity, $currentPrice);
                    }

                }
            }

            DB::commit();

            return redirect()->route('cart.index')->with('msgSuccess', 'Product added to cart');

        } catch (Exception $e) {

            DB::rollBack();
            Log::error("Error occurred during insert of order : " . $e->getMessage());

            return redirect()->back()->with('msgError', 'An error occurred, please try again');

        }
    }

    public function delete(Request $request)
    {
        $request->validate(['product_id' => 'required|exists:order_details,product_id'], $request->all());

        $productId = $request->product_id;
        $userId = session()->get('user')->id;

        $header = OrderHeader::getUnProcessed($userId)->load('orderDetails');

        OrderDetail::deleteOne($header->id, $productId);

        $header->refresh();

        if (!count($header->orderDetails)) {

            $header->delete();
            $msg = 'Your have emptied your cart';

        } else {
            $msg = 'Product is removed from cart';
        }

        return redirect()->route('cart.index')->with('msgSuccess', $msg);

    }

    public function showUnProcessedOrder()
    {
        $userId = session()->get('user')->id;
        $isPending = OrderHeader::getUnProcessed($userId);

        if ($isPending) {
            if (OrderHeader::hasProcessingTimeElapsed($isPending)) {
                OrderHeader::processOne($isPending);

            } else {
                $this->data['order'] = $isPending->load('orderDetails.product');
            }
        }

        return view('front.pages.cart', $this->data);

    }

    public function showProcessedOrders()
    {
        $userId = session()->get('user')->id;
        $pendingOrder = OrderHeader::getUnProcessed($userId);

        if ($pendingOrder) {
            if (OrderHeader::hasProcessingTimeElapsed($pendingOrder)) {
                OrderHeader::processOne($pendingOrder);
            }
        }

        $orders = OrderHeader::getProcessedOrders($userId);
        if ($orders) {
            $this->data['orders'] = $orders;
        }

        return view('front.pages.orders', $this->data);

    }

    public function showOneProcessedOrder(OrderHeader $order)
    {
        $this->data["order"] = $order;
        return view('front.pages.orderDetail', $this->data);
    }

}
