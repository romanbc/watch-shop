<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\ImageService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class BackendController extends Controller
{
    protected $data;
    protected $model;

    protected function __construct($title, $table, $limit = 6)
    {
        $this->data['title'] = $title;
        $this->data['table'] = $table;
        $this->data['what'] = ucfirst(substr($table, 0, -1));
        $this->data['limit'] = $limit;

        $this->data['links'] = [
            'icons' => [

                [
                    'route' => 'home',
                    'name' => 'Home',
                    'icon' => 'home'
                ]

            ],

            'logs' => [
                [
                    'route' => 'admin.visits.index',
                    'name' => 'Visits'
                ],
                [
                    'route' => 'admin.activities.index',
                    'name' => 'Activities'
                ]
            ],
            'orders' => [
                [
                    'route' => 'admin.orders.index',
                    'name' => 'Order History'
                ],
                [
                    'route' => 'admin.details.index',
                    'name' => 'Orders Details'
                ]
            ],
            'tables' =>
                [
                    [
                        'route' => 'admin.users.index',
                        'name' => 'Users'
                    ],
                    [
                        'route' => 'admin.menus.index',
                        'name' => 'Menus'
                    ],
                    [
                        'route' => 'admin.products.index',
                        'name' => 'Products'
                    ],
                    [
                        'route' => 'admin.brands.index',
                        'name' => 'Brands'
                    ],
                    [
                        'route' => 'admin.materials.index',
                        'name' => 'Materials'
                    ]
                ]

        ];

    }

    protected function baseIndex($columns, $editable = true, $orderBy = false, $orderDir = 'asc')
    {
        $this->data['editable'] = $editable;
        $this->data['columns'] = $columns;

        $belongs = [];

        foreach ($columns as $column) {
            if (strpos($column, '->')) {
                array_push($belongs, explode('->', $column)[0]);
            }
        }

        if (count($belongs)) {

            $items = $this->model::with($belongs);

            if ($orderBy) {
                $this->data['items'] = $items->orderBy($orderBy, $orderDir)->paginate($this->data['limit']);
            } else {
                $this->data['items'] = $items->paginate($this->data['limit']);
            }

        } else {
            if ($orderBy) {
                $this->data['items'] = $this->model::orderBy($orderBy, $orderDir)->paginate($this->data['limit']);
            } else {
                $this->data['items'] = $this->model::paginate($this->data['limit']);
            }

        }

        //https paginacija heroku
        $this->data['items'] = $this->data['items']->withPath('');

        return view('admin.pages.index', $this->data);

    }

    protected function baseDestroy($id)
    {
        try {
            $this->model::destroy($id);

            return redirect()->back()->with("msgSuccess", $this->data['what'] . "deleted successfully!");

        } catch (Exception $e) {

            Log::error("Error occurred during deletion of  " . $this->data['what'] . ": " . $e->getMessage());

            return redirect()->back()->with("msgError", "An error occurred, please try again later");
        }

    }

    protected function baseStore(Request $request)
    {
        $request->validate($this->data['rules'], $request->all());

        try {

            if ($request->hasFile('image')) {

//                $insertData = $request->except('image') + ['image' => ImageService::uploadImage($request->image)];

                $insertData = $request->except('image') + ['image' => ImageService::uploadHeroku($request->image)];

            } else if ($request->has('password')) {

                $insertData = $request->except('password') + ['password' => Hash::make($request->password)];

            } else {
                $insertData = $request->toArray();
            }

            $this->model::create($insertData);

            return redirect()->route('admin.' . $this->data['table'] . '.index')->with('msgSuccess', $this->data['what'] . ' added successfully!');

        } catch (Exception $e) {
            Log::error("Error occurred during insert of  " . $this->data['what'] . ": " . $e->getMessage());

            return redirect()->back()->with('msgError', 'An error occurred!');

        }

    }

    protected function baseUpdate(Request $request, $id)
    {
        $request->validate(isset($this->data['updateRules']) ? $this->data['updateRules'] : $this->data['rules'], $request->all());

        try {

            $item = $this->model::find($id);

            if ($item) {

                if ($request->hasFile('image')) {

//                    ImageService::deleteImage($item->image);
//                    $updateData = $request->except('image') + ['image' => ImageService::uploadImage($request->image)];

                    ImageService::deleteHeroku($item->image);
                    $updateData = $request->except('image') + ['image' => ImageService::uploadHeroku($request->image)];


                } else if ($request->has('password')) {

                    if ($request->password) {

                        $updateData = $request->except('password') + ['password' => Hash::make($request->password)];

                    } else {

                        $updateData = $request->except('password');
                    }

                } else {
                    $updateData = $request->toArray();
                }

            } else {
                return redirect()->back()->with('msgError', 'An error occurred, item does not exist!');
            }

            $item->update($updateData);

            return redirect()->route('admin.' . $this->data['table'] . '.index')->with('msgSuccess', $this->data['what'] . ' updated successfully!');

        } catch (Exception $e) {

            Log::error("Error occurred during update of  " . $this->data['what'] . ": " . $e->getMessage());
            return redirect()->back()->with('msgError', 'An error occurred!');
        }
    }
}
