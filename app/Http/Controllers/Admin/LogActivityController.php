<?php

namespace App\Http\Controllers\Admin;


use App\Models\LogActivity;


class LogActivityController extends BackendController
{
    public function __construct()
    {
        parent::__construct("Activity Logs", "Activity Logs",10);
        $this->model = new LogActivity();
    }

    public function index()
    {
        return $this->baseIndex(['user->email','action','occurred_at'],false, 'occurred_at','desc');

    }
}
