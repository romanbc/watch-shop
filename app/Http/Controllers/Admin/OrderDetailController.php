<?php

namespace App\Http\Controllers\Admin;


use App\Models\OrderDetail;

class OrderDetailController extends BackendController
{

    public function __construct()
    {
        parent::__construct("Orders Details ", "Orders  Details", 6);
        $this->model = new OrderDetail();
    }

    public function index()
    {
        return $this->baseIndex(['orderHeader->made_at','orderHeader->user->email','product->name','quantity', 'product_price','total' ], false, 'order_id', 'desc');

    }

}
