<?php

namespace App\Http\Controllers\Admin;


use App\Models\LogVisit;


class LogVisitController extends BackendController
{
    public function __construct()
    {
        parent::__construct("Visit logs", "Visit Logs",10);
        $this->model = new LogVisit();
    }

    public function index()
    {
        return $this->baseIndex(['ip','path','visited_at'],false, 'visited_at','desc');
    }
}
