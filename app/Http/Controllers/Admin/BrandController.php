<?php

namespace App\Http\Controllers\Admin;


use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends BackendController
{
    public function __construct()
    {
        parent::__construct("Brand management", "brands", 8);
        $this->model = new Brand();
        $this->data['fields'] = [
            [
                'type' => 'text',
                'column' => 'name',
                'text' => 'Name'
            ]
        ];

        $this->data['rules'] = [
            'name' => 'required|string|max:100'
        ];

    }

    public function index()
    {
        return $this->baseIndex(['name'], true, 'name');
    }

    public function create()
    {
        return view('admin.pages.create', $this->data);
    }


    public function store(Request $request)
    {
        return $this->baseStore($request);
    }

    public function edit(Brand $brand)
    {
        $this->data['item'] = $brand->toArray();
        return view('admin.pages.edit', $this->data);
    }


    public function update(Request $request, $id)
    {
        return $this->baseUpdate($request, $id);
    }


    public function destroy($id)
    {
        return $this->baseDestroy($id);
    }

}
