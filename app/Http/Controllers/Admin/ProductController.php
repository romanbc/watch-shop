<?php

namespace App\Http\Controllers\Admin;


use App\Models\Brand;
use App\Models\Material;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends BackendController
{

    public function __construct()
    {
        parent::__construct("Product management", "products");
        $this->model = new Product();

        $brands = Brand::all()->toArray();
        $materials = Material::all()->toArray();

        $this->data['fields'] = [
            [
                'type' => 'text',
                'column' => 'name',
                'text' => 'Name'
            ],
            [
                'type' => 'textarea',
                'column' => 'description',
                'text' => 'Description'
            ],
            [
                'type' => 'number',
                'column' => 'price',
                'text' => 'Price'
            ],
            [
                'type' => 'number',
                'column' => 'discount',
                'text' => ' Percentage Discount'
            ],
            [
                'type' => 'radio',
                'column' => 'gender',
                'text' => 'Gender',
                'pairs' => [
                    [
                        'value' => 'male',
                        'text' => 'Male'
                    ],
                    [
                        'value' => 'female',
                        'text' => 'Female'
                    ],
                    [
                        'value' => 'unisex',
                        'text' => 'Unisex'
                    ]
                ]
            ],
            [
                'type' => 'select',
                'column' => 'brand_id',
                'text' => 'Brand',
                'pairs' => $brands
            ],
            [
                'type' => 'select',
                'column' => 'material_id',
                'text' => 'Material',
                'pairs' => $materials
            ],
            [
                'type' => 'file',
                'column' => 'image',
                'text' => 'Image'

            ]
        ];

        $this->data['rules'] = [
            'name' => 'required|string|max:150',
            'description' => 'required|string',
            'price' => 'required|numeric|min:0',
            'discount' => 'required|numeric|min:0',
            'gender' => 'required|in:male,female,unisex',
            'image' => 'required|image|max:2000',
            'brand_id' => 'exists:brands,id',
            'material_id' => 'exists:materials,id'

        ];

        $this->data['updateRules'] = $this->data['rules'];
        $this->data['updateRules']['image'] = 'nullable|image|max:2000';

    }

    public function index()
    {
        return $this->baseIndex(['name', 'price', 'discount', 'gender', 'brand->name', 'material->name'], true, 'name');

    }

    public function create()
    {
        return view('admin.pages.create', $this->data);
    }

    public function store(Request $request)
    {
        return $this->baseStore($request);
    }


    public function edit(Product $product)
    {
        $this->data['item'] = $product->toArray();
        return view('admin.pages.edit', $this->data);
    }


    public function update(Request $request, $id)
    {
        return $this->baseUpdate($request, $id);
    }

    public function destroy($id)
    {
        return $this->baseDestroy($id);
    }
}
