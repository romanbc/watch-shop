<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends BackendController
{
    public function __construct()
    {
        parent::__construct("User management", "users");
        $this->model = new User();

        $this->data['fields'] = [
            [
                'type' => 'text',
                'column' => 'email',
                'text' => 'Email'
            ],
            [
                'type' => 'text',
                'column' => 'first_name',
                'text' => 'First Name'
            ],
            [
                'type' => 'text',
                'column' => 'last_name',
                'text' => 'Last Name'
            ],
            [
                'type' => 'password',
                'column' => 'password',
                'text' => 'Password'
            ]
            , [
                'type' => 'radio',
                'column' => 'role',
                'text' => 'Role',
                'pairs' => [
                    [
                        'value' => 'admin',
                        'text' => 'Admin'
                    ],
                    [
                        'value' => 'customer',
                        'text' => 'Customer'
                    ]
                ]
            ]
        ];

        $this->data['rules'] = [
            'first_name' => 'required|alpha|min:2|max:60',
            'last_name' => 'required|alpha|min:2|max:60',
            'email' => 'required|email|unique:users',
            'password' => [
                'required',
                "regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/"
            ],
            'role' => 'required|in:customer,admin',

        ];

        $this->data['updateRules'] = $this->data['rules'];
        $this->data['updateRules']['password'] = [
            'nullable'
            , "regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/"
        ];

    }

    public function index()
    {
        return $this->baseIndex(['email', 'role', 'registered_at', 'first_name', 'last_name'], true, 'email');

    }

    public function create()
    {
        return view('admin.pages.create', $this->data);
    }

    public function store(Request $request)
    {
        return $this->baseStore($request);
    }

    public function edit(User $user)
    {
        $this->data['item'] = $user->makeHidden('password')->toArray();
        return view('admin.pages.edit', $this->data);
    }

    public function update(Request $request, $id)
    {
        $this->data['updateRules']['email'] = 'required|email|unique:users,email,' .  $id;
        return $this->baseUpdate($request, $id);
    }

    public function destroy($id)
    {
        return $this->baseDestroy($id);
    }

}
