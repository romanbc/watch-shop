<?php

namespace App\Http\Controllers\Admin;


use App\Models\Menu;
use Illuminate\Http\Request;


class MenuController extends BackendController
{
    public function __construct()
    {

        parent::__construct("Menu management", "menus");

        $this->model = new Menu();

        $this->data['fields'] = [
            [
                'type' => 'text',
                'column' => 'name',
                'text' => 'Name'
            ],
            [
                'type' => 'text',
                'column' => 'route',
                'text' => 'Route'
            ],
            [
                'type' => 'number',
                'column' => 'order',
                'text' => 'Order'
            ]
        ];

        $this->data['rules'] = [
            'name' => 'required|string|max:100',
            'route' => 'required|string|max:100',
            'order' => 'required|numeric|integer|min:0'
        ];

    }


    public function index()
    {
        return $this->baseIndex(['name', 'route', 'order']);
    }


    public function create()
    {
        return view('admin.pages.create', $this->data);
    }


    public function store(Request $request)
    {
        return $this->baseStore($request);
    }


    public function edit(Menu $menu)
    {
        $this->data['item'] = $menu->toArray();
        return view('admin.pages.edit', $this->data);
    }


    public function update(Request $request, $id)
    {
        return $this->baseUpdate($request, $id);
    }


    public function destroy($id)
    {
        return $this->baseDestroy($id);
    }

}
