<?php

namespace App\Http\Controllers\Admin;


use App\Models\OrderHeader;

class OrderHeaderController extends BackendController
{

    public function __construct()
    {
        parent::__construct("Order History", "Order History", 6);
        $this->model = new OrderHeader();
    }

    public function index()
    {
        return $this->baseIndex(['user->email', 'processed', 'made_at','total'], false, 'made_at', 'desc');

    }

}
