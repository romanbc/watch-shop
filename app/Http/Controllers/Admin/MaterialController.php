<?php

namespace App\Http\Controllers\Admin;


use App\Models\Material;
use Illuminate\Http\Request;


class MaterialController extends BackendController
{

    public function __construct()
    {
        parent::__construct("Material management", "materials", 8);

        $this->model = new Material();

        $this->data['fields'] = [
            [
                'type' => 'text',
                'column' => 'name',
                'text' => 'Name'
            ]
        ];

        $this->data['rules'] = [
            'name' => 'required|string|max:100'
        ];

    }


    public function index()
    {
        return $this->baseIndex(['name'], true, 'name');
    }

    public function create()
    {
        return view('admin.pages.create', $this->data);
    }


    public function store(Request $request)
    {
        return $this->baseStore($request);
    }


    public function edit(Material $material)
    {
        $this->data['item'] = $material->toArray();
        return view('admin.pages.edit', $this->data);
    }


    public function update(Request $request, $id)
    {
        return $this->baseUpdate($request, $id);
    }


    public function destroy($id)
    {
        return $this->baseDestroy($id);
    }
}
