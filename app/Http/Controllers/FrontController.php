<?php

namespace App\Http\Controllers;


use App\Models\Menu;


class FrontController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data["menu"] = Menu::getMenu();
    }

    public function home()
    {

        $this->data['products'] = ProductController::getBestDeals();

        return view("front.pages.home", $this->data);
    }

    public function author()
    {
        return view("front.pages.author", $this->data);
    }

    public function login()
    {
        return view("front.pages.login", $this->data);
    }

    public function register()
    {
        return view("front.pages.register", $this->data);
    }

}
