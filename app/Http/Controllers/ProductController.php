<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Material;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends FrontController
{

    const PAGINATION_LIMIT = 6;

    public static function getBestDeals($limit = 8)
    {
        return Product::with('brand')->orderBy('discount', 'desc')->orderBy('price', 'desc')->paginate($limit);
    }

    public function index()
    {
        $this->data['genders'] = ['male', 'female', 'unisex'];
        $this->data['materials'] = Material::all();
        $this->data['brands'] = Brand::all();

        $this->data['products'] = Product::paginate(self::PAGINATION_LIMIT);

        return view("front.pages.products", $this->data);
    }

    public function show(Product $product)
    {

        $this->data['product'] = $product;
        return view("front.pages.product", $this->data);
    }

    public function getFiltered(Request $request)
    {

        try {

            $gender = $request->gender;
            $search = $request->search;
            $brands = $request->brands;
            $materials = $request->materials;
            $sortValue = $request->sortValue;

            $productsData = Product::getProducts($gender, $brands, $materials, $search, $sortValue)->paginate(self::PAGINATION_LIMIT);

            return response()->json($productsData);

        } catch (\Exception $e) {
            \Log::error('Error occurred while fetching products: ' . $e->getMessage());

            return response(["msgError" => "Server Error occurred"], 500);
        }


    }

}
