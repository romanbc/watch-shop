<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class LogActivity extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ["user_id", "action"];


    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public static function log($id, $action)
    {
        try {
            LogActivity::create([
                "user_id" => $id,
                "action" => $action
            ]);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

}
