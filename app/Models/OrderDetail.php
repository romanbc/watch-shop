<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = ['order_id', 'product_id', 'quantity', 'product_price'];

    public function orderHeader()
    {
        return $this->belongsTo(OrderHeader::class, 'order_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getTotalAttribute()
    {
        return $this->total();
    }

    public function total()
    {
        return $this->product_price * $this->quantity;
    }

    public static function deleteOne($orderId, $productId)
    {
          OrderDetail::where([
              ['order_id',$orderId],
              ['product_id',$productId]
          ])->delete();
    }


    public static function addNewOne($orderId, $productId, $quantity, $price)
    {
        $detail = new OrderDetail();
        $detail->order_id = $orderId;
        $detail->product_id = $productId;
        $detail->quantity = $quantity;
        $detail->product_price = $price;

        $detail->save();
    }

    public static function doesOrderAlreadyHaveProduct($orderId, $productId)
    {
        return OrderDetail::where([
            ['order_id', $orderId],
            ['product_id', $productId]
        ])->first();

    }

    public static function changeProductQuantity($orderDetail, $quantity)
    {
        $orderDetail->quantity = $quantity;
        $orderDetail->save();
    }
}
