<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderHeader extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = ['user_id', 'processed', 'made_at'];

    protected $dates = [
        'made_at',
        'deleted_at'
    ];


    private const PROCESSING_TIME = 10;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'order_id',);
    }

    public function getTotalAttribute()
    {
        return $this->total();
    }

    public function total()
    {
        $total = 0;

        foreach ($this->orderDetails as $d) {
            $total += $d->total();
        }

        return $total;
    }


    public function getDate($timezone = 'Europe/Belgrade'){
        return $this->made_at->setTimeZone($timezone);
    }

    public function remainingTime( $minutes = self::PROCESSING_TIME)
    {
        return $this->getDate()->addMinutes($minutes);

    }

    public static function getUnProcessed($userId)
    {
        return OrderHeader::where([
            ['user_id', $userId],
            ['processed', false]
        ])->first();
    }

    public static function getProcessedOrders($userId)
    {
        return OrderHeader::with(['orderDetails.product'])
            ->where([
                ['user_id', $userId],
                ['processed', true]
            ])
            ->get();
    }

    public static function hasProcessingTimeElapsed($orderHeader, $minutes = self::PROCESSING_TIME)
    {

        return $orderHeader->made_at < now()->subMinutes($minutes);
    }


    public static function processOne($orderHeader)
    {
        $orderHeader->processed = true;
        $orderHeader->save();
    }

    public static function addNewOne($userId)
    {
        $header = new OrderHeader();
        $header->user_id = $userId;
        $header->made_at = now();
        $header->save();

        return $header;
    }

}
