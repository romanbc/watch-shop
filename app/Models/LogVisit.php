<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Log;

class LogVisit extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ["ip", "path"];

    public static function log($path, $ip) {
        try {
            LogVisit::create([
                "path" => $path,
                "ip" => $ip
            ]);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

}
