<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $timestamps = false;

    protected $fillable = ["name", "description", "price", "discount", "gender", "image", "brand_id", "material_id"];

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public static function getProducts($gender, $brands, $materials, $search, $sortValue)
    {

        $products = Product::with(['brand', 'material']);

        if ($gender) {
            $products = $products->where('gender', $gender);
        }


        if (is_array($brands)) {
            $products = $products->whereIn('brand_id', $brands);
        }

        if (is_array($materials)) {
            $products = $products->whereIn('material_id', $materials);
        }

        if ($search) {
            $products = $products->where('name', 'LIKE', "%{$search}%");
        }

        if ($sortValue) {

            $products->orderByRaw('ROUND ((price * (100 - discount) / 100), 2) ' . $sortValue);
        }

        return $products;
    }

    public static function getCurrentPrice($id)
    {
        $product = Product::find($id)->only(['price', 'discount']);
        return $product['discount'] > 0 ? round(($product['price'] * (100 - $product['discount']) / 100), 2) : $product['price'];

    }

}
