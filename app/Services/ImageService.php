<?php


namespace App\Services;


use Illuminate\Support\Facades\Storage;

class ImageService
{


    public static function uploadImage($image)
    {
        $path = Storage::disk('public')->putFile('img/products', $image);
        $exploded = explode('/', $path);
        return $exploded[count($exploded) - 1];
    }

    public static function deleteImage($image)
    {
        Storage::disk('public')->delete('img/products/' . $image);
    }

    public static function uploadHeroku($image)
    {
        $fileName = time() . "_" . $image->getClientOriginalName();
        $image->move("./img/products", $fileName);

        return $fileName;
    }

    public static function deleteHeroku($image)
    {
        unlink( './img/products/' . $image);
    }

}
