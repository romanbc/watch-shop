<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    private $products = [
        [
            'name' => 'Casio Collection Watch MWA-100HD-1AVEF',
            'description' => "Casio Edifice EFV-110D-1AVUEF is an amazing and handsome Gents watch. Case material is Stainless Steel, which stands for a high quality of the item and the Black dial gives the watch that unique look. The features of the watch include (among others) a chronograph. This model has 100 metres water resistancy - it is suitable for swimming, but not high impact. We ship it with an original box and a guarantee from the manufacturer.",
            'gender' => 'male',
            'price' => '50',
            'discount' => '10',
            'image' => '1615800144_MWA-100HD-1AVEF.jpg',
            'material' => 'Metal',
            'brand' => 'Casio'
        ],
        [
            'name' => 'Digital Camo Watch GW-B5600DC-1ER',
            'description' => "Blending tough styling with solid performance and splashes of colour, the Casio Digital Camo Watch delivers every time.
This versatile Casio G-Shock is built to last, and is the ideal choice if you’re always on the go. Five daily alarms and a 24-hour countdown timer keep you ready for anything, while hardwearing mineral glass and double LED lighting ensure great looks every time. There’s also a world time feature for jet-setters, and shock resistance means it can withstand hard knocks. It’s water resistant to 200 metres, too, so use it in the pool or on the beach.",
            'gender' => 'male',
            'price' => '150',
            'discount' => '20',
            'image' => '1615801115_GW-B5600DC-1ER.jpg',
            'material' => 'Plastic',
            'brand' => 'Casio'
        ],
        [
            'name' => 'Hugo Boss Watch 1502564',
            'description' => "Hugo Boss Hera 1502564 is an attractive watch from Hera collection. Case material is Plated Stainless Steel while the dial colour is Silver. This model has got 30 metres water resistancy - it can be worn in scenarios where it is likely to be splashed but not immersed in water. It can be worn while washing your hands and will be fine in rain. We ship it with an original box and a guarantee from the manufacturer.",
            'gender' => 'female',
            'price' => '260',
            'discount' => '0',
            'image' => '1615800372_HB-1502564.jpg',
            'material' => 'Metal',
            'brand' => 'Boss'
        ],
        [
            'name' => 'Tissot Watch T1204171704100',
            'description' => "From the T-Sport Collection, the Tissot Seastar 1000 Watch is the timepiece to choose if you’re mad about scuba diving and intense water sports.
Water resistant to a jaw-dropping 300 metres, this watch is the ideal accessory if you’re serious about diving down into the depths. It’s well protected by sapphire glass, as well as a screw-down crown and case back. There’s a three-subdial chronograph to track hours, minutes and seconds, and a date window between 4 and 5 o’clock. ",
            'gender' => 'male',
            'price' => '500',
            'discount' => '30',
            'image' => '1615801783_T120_417_17_041_00.jpg',
            'material' => 'Rubber',
            'brand' => 'Tissot'
        ],
        [
            'name' => 'Pocket Savonette Watch T8624102901300',
            'description' => "Tissot Pocket Savonette T8624102901300 is a great Pocket watch. Material of the case is Two-Tone Steel and Rose Plate, which stands for a high quality of the item while the dial colour is White. The features of the watch include (among others) a date function. We ship it with an original box and a guarantee from the manufacturer.",
            'gender' => 'unisex',
            'price' => '300',
            'discount' => '40',
            'image' => '1615801516_T8624102901300.jpg',
            'material' => 'Metal',
            'brand' => 'Tissot'
        ],
        [
            'name' => 'Seiko Watch SRPE80K1',
            'description' => "Seiko SRPE80K1 is an amazing Unisex watch. Material of the case is Stainless Steel while the dial colour is Black. This model has 100 metres water resistancy - it is suitable for swimming, but not high impact. The watch is shipped with an original box and a guarantee from the manufacturer.",
            'gender' => 'unisex',
            'price' => '600',
            'discount' => '20',
            'image' => '1615801720_SRPE80K1.jpg',
            'material' => 'Rubber',
            'brand' => 'Seiko'
        ],
        [
            'name' => 'Ladies Seiko Automatic Watch SRP852J1',
            'description' => "Seiko SRP852J1 is an amazing and attractive Ladies watch. Case is made out of Stainless Steel and the Champagne dial gives the watch that unique look. This model has 100 metres water resistancy - it is suitable for swimming, but not high impact. The watch is shipped with an original box and a guarantee from the manufacturer.",
            'gender' => 'female',
            'price' => '150',
            'discount' => '20',
            'image' => '1615791233_SRP852J1.jpg',
            'material' => 'Leather',
            'brand' => 'Seiko'
        ],
        [
            'name' => 'Certina Watch C0334511603100',
            'description' => "Certina DS8 C0334511603100 is a functional and special Gents watch. Case material is Stainless Steel while the dial colour is Silver. The features of the watch include (among others) a date function. In regards to the water resistance, the watch has a water resistance of 100 metres. This makes it suitable for swimming, but not high impact water sports. The watch is shipped with an original box and a guarantee from the manufacturer.",
            'gender' => 'male',
            'price' => '450',
            'discount' => '0',
            'image' => '1615801426_C0334511603100.jpg',
            'material' => 'Leather',
            'brand' => 'Certina'
        ],
        [
            'name' => 'Certina Watch C0330512212800',
            'description' => " Certina DS-8 Lady C0330512212800 is a functional and very impressive Gents watch from DS-8 Lady collection. Case is made out of Stainless Steel while the dial colour is Blue. The features of the watch include (among others) a date function. In regards to the water resistance, the watch has a water resistance of 100 metres. This makes it suitable for swimming, but not high impact water sports. The watch is shipped with an original box and a guarantee from the manufacturer.",
            'gender' => 'female',
            'price' => '500',
            'discount' => '25',
            'image' => '1615801656_C0330512212800.jpg',
            'material' => 'Metal',
            'brand' => 'Certina'
        ],
        [
            'name' => 'Hugo Boss Watch 1502587',
            'description' => "Hugo Boss 1502587 is an incredible eye-catching Ladies watch. Case is made out of Stainless Steel. 30 metres water resistancy will protect the watch and allows it to be worn in scenarios where it is likely to be splashed but not immersed in water. It can be worn while washing your hands and will be fine in rain. We ship it with an original box and a guarantee from the manufacturer.",
            'gender' => 'female',
            'price' => '360',
            'discount' => '30',
            'image' => '1615801337_1502587.jpg',
            'material' => 'Metal',
            'brand' => 'Boss'
        ],
        [
            'name' => 'Chronograph Solar Powered Watch SSH067J1',
            'description' => "Seiko SSH067J1 is an amazing and attractive Gents watch. Case material is Stainless Steel while the dial colour is Black. The features of the watch include (among others) a chronograph. This model has got 200 metres water resistancy - it can be used for professional marine activity, skin diving and high impact water sports, but not deep sea or mixed gas diving. The watch is shipped with an original box and a guarantee from the manufacturer.",
            'gender' => 'male',
            'price' => '3500',
            'discount' => '40',
            'image' => '1615801772_SSH067J1.jpg',
            'material' => 'Metal',
            'brand' => 'Seiko'
        ],
        [
            'name' => 'Edifice Sapphire Solar Retrograde Watch EFS-S540DB-1AUEF',
            'description' => "Part of the Casio Edifice Collection, the Gents Casio Edifice Sapphire Solar Retrograde Watch is a high-class accessory that combines sharp looks with feature-packed performance.The Edifice uses solar power, making it more green-friendly and eliminating the need for expensive batteries. It's tough enough to withstand swimming and snorkelling, thanks to 100-metre water resistance, and sapphire glass keeps it looking good at all times.",
            'gender' => 'male',
            'price' => '230',
            'discount' => '0',
            'image' => '1615801284_EFS-S540DB-1AUEF.jpg',
            'material' => 'Metal',
            'brand' => 'Casio'
        ],
        [
            'name' => 'Tissot Heritage 1948 Automatic Chronograph Watch T66171233',
            'description' => "This is not a conventional watch. When purchasing this model, you own a piece of Tissot's Heritage. This automatic chronograph incorporates the very best of past and present technology. While the leather strap versions are more classical, the milanese bracelet gives it a contemporary touch - a Must have for watch lovers and collectors.",
            'gender' => 'male',
            'price' => '2300',
            'discount' => '40',
            'image' => '1615801822_T66171233.jpg',
            'material' => 'Leather',
            'brand' => 'Tissot'
        ],
        [
            'name' => 'DS Queen Lady Watch C0182101642700',
            'description' => "From innovative watchmaker Certina, the DS Queen Lady will make any woman feel like royalty. The regal red dial, akin to a rose, is set in a polished stainless steel case and encircled in a red ring, providing some extra interest. Continuing on the red colour scheme, the fashionable leather strap is embossed with a crocodile-style pattern and bold red stitching. A true star from Certina.",
            'gender' => 'female',
            'price' => '300',
            'discount' => '10',
            'image' => '1615801616_C0182101642700.jpg',
            'material' => 'Leather',
            'brand' => 'Certina'
        ],
        [
            'name' => 'Casio Watch B640WDG-7EF',
            'description' => "The Unisex Casio Vintage Glitter Alarm Chronograph Watch is ideal if you’re after a watch with the wow factor.The watch is water resistant to 50 metres, so you can go swimming with it on in shallow waters. There’s also a 24-hour stopwatch and countdown timer, which is ideal when you’re on a training run. The day and date are always on show, plus with the multi-alarm feature, you’ll never be late again. And when the sun goes down, the display is lit up at the touch of a button. ",
            'gender' => 'male',
            'price' => '60',
            'discount' => '10',
            'image' => '1615801593_B640WDG-7EF.jpg',
            'material' => 'Metal',
            'brand' => 'Casio'
        ]
    ];


    public function run()
    {

        $materials = \App\Models\Material::all();
        $brands = \App\Models\Brand::all();

        foreach ($this->products as $p) {
            $id = \DB::table('products')->insertGetId([
                'name' => $p['name'],
                'description' => $p['description'],
                'gender' => $p['gender'],
                'price' => $p['price'],
                'discount' => $p['discount'],
                'image' => $p['image'],
                'material_id' => $materials->where('name', $p['material'])->first()->id,
                'brand_id' => $brands->where('name', $p['brand'])->first()->id
            ]);
        }

    }
}
