<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $materials = ['Leather', 'Metal', 'Rubber','Plastic'];

    public function run()
    {
        foreach ($this->materials as $material) {
            \DB::table('materials')->insert([
                'name' => $material
            ]);
        }
    }
}
