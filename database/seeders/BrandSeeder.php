<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $brands = ['Tissot', 'Seiko', 'Certina','Casio','Boss'];

    public function run()
    {
        foreach ($this->brands as $brand) {
            \DB::table('brands')->insert([
                'name' => $brand
            ]);
        }
    }
}
