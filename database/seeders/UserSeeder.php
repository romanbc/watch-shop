<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */



    private $users = [
        [
            'email' => 'pera',
            'first_name' => 'Pera',
            'last_name' => 'Jović',
            'role' => 'customer'
        ],
        [
            'email' => 'ana',
            'first_name' => 'Ana',
            'last_name' => 'Prica',
            'role' => 'admin'
        ],
        [
            'email' => 'jovo',
            'first_name' => 'Jovo',
            'last_name' => 'Popara',
            'role' => 'admin'
        ],
        [
            'email' => 'mmarko',
            'first_name' => 'Marko',
            'last_name' => 'Marković',
            'role' => 'customer'
        ]

    ];


    public function run()
    {


        foreach ($this->users as $u) {
            $id = \DB::table('users')->insert([
                'email' => $u['email'] . '@gmail.com',
                'password' => Hash::make(env('MOJ_PASSWORD_' . strtoupper($u['role']))),
                'first_name' => $u['first_name'],
                'last_name' => $u['last_name'],
                'role' => $u['role'],
            ]);
        }
    }
}
