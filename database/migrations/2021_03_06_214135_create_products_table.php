<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->id();
            $table->string('name', 150);
            $table->text('description');
            $table->decimal('price', 12, 2);
            $table->decimal('discount', 5, 2)->default(0);
            $table->string('image', 255);
            $table->enum('gender', ['male', 'female', 'unisex']);

            $table->foreignId('brand_id')->nullable()->constrained()->onUpdate('cascade')->onDelete('set null');
            $table->foreignId('material_id')->nullable()->constrained()->onUpdate('cascade')->onDelete('set null');

            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
