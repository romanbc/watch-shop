<!--================ Start Header Menu Area =================-->
<header class="header_area">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand logo_h" href="{{route('home')}}"><img src="{{ asset("img/mylogo.png") }}"
                                                                             alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">

                    <ul class="nav navbar-nav menu_nav ml-auto mr-auto ">

                        @foreach($menu as $link)
                            <li class="nav-item @if(request()->routeIs($link->route)) active @endif">
                                <a class="nav-link" href="{{route($link->route)}}">{{$link->name}}</a>
                            </li>
                        @endforeach

                        <li class="nav-item submenu dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">Account</a>
                            <ul class="dropdown-menu">

                                @if(!session()->has('user'))
                                    <li class="nav-item"><a class="nav-link" href="{{route('form.login')}}">Login</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link"
                                                            href="{{route('form.register')}}">Register</a>
                                    </li>
                                @else
                                    @if(session("user")->role == "customer")
                                        <li class="nav-item"><a class="nav-link" href="{{url('/cart')}}">Cart</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" href="{{route('orders.list')}}">Order History</a>
                                        </li>
                                    @else
                                        <li class="nav-item"><a class="nav-link" href="{{url('/admin')}}">Admin</a></li>
                                    @endif
                                    <li class="nav-item"><a class="nav-link" href="{{url('/logout')}}">Logout</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </div>
</header>
<!--================ End Header Menu Area =================-->
