<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Watch Shop - @yield('title')</title>

    <link rel="icon" href="{{ asset("img/favicon.png") }}" type="image/png">

    @section('cssVendors')
        <link rel="stylesheet" href="{{ asset("vendors/bootstrap/bootstrap.min.css")}}">
        <link rel="stylesheet" href="{{ asset("vendors/fontawesome/css/all.min.css")}}">
        <link rel="stylesheet" href="{{ asset("vendors/themify-icons/themify-icons.css")}}">

    @show
    <link rel="stylesheet" href="{{ asset("css/style.css") }}">


</head>

