<!--================ Start footer Area  =================-->
<footer class="footer">
    <div class="footer-bottom">
        <div class="container">
            <div class="row d-flex">
                <p class="col-lg-4 footer-text text-center">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                    All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by
                    <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>


                @foreach($menu as $link)
                    <p class="col-lg-1 footer_title text-center ml-auto">
                        <a href="{{route($link->route)}}">{{$link->name}}</a>
                    </p>
                @endforeach

                <p class="col-lg-1 footer_title text-center ml-auto">
                    <a href="{{route('author')}}">Autor</a>
                </p>
                <p class="col-lg-1 footer_title text-center ml-auto">
                    <a href="{{ url('dokumentacija.pdf') }} " target="_blank">Dokumentacija</a>
                </p>


            </div>
        </div>
    </div>
</footer>
<!--================ End footer Area  =================-->
