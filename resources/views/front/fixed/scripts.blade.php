@section('jsVendors')

    <button type="button" id="toTop" title="To top">
        <i class="fas fa-arrow-up"></i>
    </button>

    <script src="{{ asset("vendors/jquery/jquery-3.2.1.min.js")}} "></script>
    <script src="{{ asset("vendors/bootstrap/bootstrap.bundle.min.js")}}"></script>

    <script src="{{asset("vendors/skrollr.min.js")}}"></script>


@show
<script src="{{ asset("js/common.js")}}"></script>

