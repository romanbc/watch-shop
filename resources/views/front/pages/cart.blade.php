@extends('layouts.front')
@section('title')
    Cart
@endsection

@section('currentPage')
    Shopping Cart
@endsection

@section('main')

    @include('front.fixed.breadcrumbs')

    @include('msg')

    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">

                @if(isset($order))

                    <div class="table-responsive">
                        <table class="table ">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Product</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Total</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($order->orderDetails as $d)
                                @component("front.components.orderItem", ["d" => $d, "editable" => true ])
                                @endcomponent
                            @endforeach


                            <tr>
                                <td colspan="3">
                                    Order will be placed at: {{$order->remainingTime()}} CET
                                </td>
                                <td>
                                    <h5>Subtotal</h5>
                                </td>
                                <td>
                                    <h5>${{$order->total()}}</h5>
                                </td>
                            </tr>

                            <tr class="out_button_area">
                                <td colspan="5">
                                    <a class="gray_btn" href="{{route('products.index')}}">Continue
                                        Shopping</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>

                @else
                    <div style="height: 20em;" class="text-center">
                        <h5 class=" text-warning py-5 ">Your cart is empty</h5>
                        <a class="gray_btn " href="{{route('products.index')}}">Continue
                            Shopping</a>
                    </div>
                @endif

            </div>


        </div>
    </section>
    <!--================End Cart Area =================-->

@endsection


