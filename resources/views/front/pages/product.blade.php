@extends('layouts.front')
@section('title')
    Product
@endsection

@section('cssVendors')
    @parent
    <link rel="stylesheet" href="{{ asset("vendors/nice-select/nice-select.css")}}">
    <link rel="stylesheet" href="{{ asset("vendors/nouislider/nouislider.min.css")}}">

@endsection
@section('jsVendors')
    @parent

    <script src="{{ asset("vendors/nice-select/jquery.nice-select.min.js")}}"></script>
    <script src="{{ asset("vendors/nouislider/nouislider.min.js")}}"></script>

@endsection

@section('currentPage')
    Single Product
@endsection


@section('main')

    @include('front.fixed.breadcrumbs')

    @include('msg')

    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-6">
                    <div class="owl-carousel owl-theme s_Product_carousel">
                        <div class="single-prd-item">

                            <img class="img-fluid" src="{{asset('img/products/' . $product->image)}}"
                                 alt="image_of_{{$product->name}}">
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <div class="s_product_text">
                        <h3>{{$product->name}}</h3>
                        <h2>
                            <div class="discounted-price">
                                @if($product->discount > 0)
                                    <del>
                                        ${{$product->price}}
                                    </del>
                                    <ins>
                                        ${{ round(($product->price * (100 - $product->discount) / 100), 2) }}
                                    </ins>
                                    <p>Save {{round($product->discount)}}% today</p>
                                @else
                                    <ins>
                                        ${{$product->price}}
                                    </ins>
                                @endif
                            </div>


                        </h2>

                        <ul class="list">

                            <li><span>Brand</span> : {{$product->brand->name}}</li>
                            <li><span>Band Material</span> : {{$product->material->name}}</li>
                            <li><span>Availibility</span> : In Stock</li>

                        </ul>
                        <p>{{$product->description}}</p>

                        @if(session()->has('user'))
                            @if(session("user")->role === "customer")

                                <div>
                                    <form method="POST" action="{{route('cart.add')}}">
                                        @csrf

                                        <input type="hidden" name="product_id" value="{{$product->id}}" />
                                        <label for="quantity">Quantity:</label>
                                        <input type="number" value="1" min="1"  id="quantity" name="quantity"
                                               size="3"/>
                                        <button type="submit" class="button primary-btn ml-2">Add to Cart</button>
                                    </form>
                                </div>
                            @else
                                <div class="product_count">
                                    <p class="text-info">Only customers can buy products</p>
                                </div>
                            @endif

                        @else
                            <div class="product_count">
                                <p class="text-info">Only registered customers can buy products</p>
                                <a class="button primary-btn" href="{{route('form.login')}}">Log in</a>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->

    <div class="mb-5"></div>

@endsection
