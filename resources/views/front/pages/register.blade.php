@extends('layouts.front')
@section('title')
    Register
@endsection

@section('currentPage')
    Register
@endsection

@section('main')

    @include('front.fixed.breadcrumbs')

    <!--================Login Box Area =================-->
    <section class="login_box_area section-margin">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login_box_img">
                        <div class="hover">
                            <h4>Already have an account?</h4>
                            <p>You are going to need an account in order to be able to buy our products.</p>
                            <a class="button button-account" href="{{route('form.login')}}">Login Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login_form_inner register_form_inner">

                        @include('msg')

                        <h3>Create an account</h3>
                        <form class="row login_form" action="{{route('register')}}" id="register_form" method="POST">
                            @csrf
                            <div class="col-md-12 form-group">
                                <input type="email" class="form-control" name="email"
                                       placeholder="Email Address" onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = 'Email Address'" value="{{old('email')}}">
                                @error('email')
                                <div class="text-danger">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" name="first_name" placeholder="First name"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" value="{{old('first_name')}}">
                                @error('first_name')
                                <div class="text-danger">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" name="last_name" placeholder="Last name"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" value="{{old('last_name')}}">
                                @error('last_name')
                                <div class="text-danger">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" name="password"
                                       placeholder="Password" onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = 'Password'">
                                @error('password')
                                <div class="text-danger">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>

                            <div class="col-md-12 form-group">
                                <button type="submit" value="submit" class="button button-register w-100">Register
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Login Box Area =================-->

@endsection

