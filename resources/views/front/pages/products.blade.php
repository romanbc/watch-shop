@extends('layouts.front')
@section('title')
    Products
@endsection

@section('cssVendors')
    @parent
    <link rel="stylesheet" href="{{ asset("vendors/nice-select/nice-select.css")}}">
    <link rel="stylesheet" href="{{ asset("vendors/nouislider/nouislider.min.css")}}">

@endsection

@section('jsVendors')
    @parent

    <script src="{{ asset("vendors/nice-select/jquery.nice-select.min.js")}}"></script>
    <script src="{{ asset("vendors/nouislider/nouislider.min.js")}}"></script>

    <script type="text/javascript">
        const baseUrl = "{{url('/')}}";
        const publicFolder = "{{asset('/')}}";
    </script>

    <script src="{{ asset("js/products.js") }}"></script>
@endsection

@section('currentPage')
    Shop Products
@endsection


@section('main')

    @include('front.fixed.breadcrumbs')

    @include('msg')

    <!-- ================ category section start ================= -->
    <section class="section-margin--small mb-5">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-5">

                    <div class="sidebar-categories">
                        <div class="head">Gender</div>
                        <ul class="main-categories">
                            <li class="common-filter">
                                <ul>

                                    @foreach($genders as $g)

                                        <li class="filter-list"><input class="pixel-radio" type="radio"
                                                                       id="gender-{{$g}}"
                                                                       name="gender" value="{{$g}}"><label
                                                for="gender-{{$g}}">{{ucfirst($g)}}</label>
                                        </li>
                                    @endforeach

                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="sidebar-categories">
                        <div class="head">Brand</div>
                        <ul class="main-categories">
                            <li class="common-filter">

                                <ul>
                                    @foreach($brands as $b )

                                        <li class="filter-list">
                                            <input class="pixel-checkbox m-r-5 " type="checkbox" name="brands"
                                                   value="{{$b->id}}" id="brands-{{$b->id}}"
                                            >
                                            <label
                                                for="brands-{{$b->id}}">{{$b->name}}</label></li>


                                    @endforeach


                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="sidebar-categories">
                        <div class="head">Band Material</div>
                        <ul class="main-categories">
                            <li class="common-filter">

                                <ul>
                                    @foreach($materials as $m)

                                        <li class="filter-list">
                                            <input class="pixel-checkbox m-r-5" type="checkbox" name="materials"
                                                   value="{{$m->id}}" id="materials-{{$m->id}}">
                                            <label
                                                for="materials-{{$m->id}}">{{$m->name}}</label>

                                        </li>

                                    @endforeach

                                </ul>
                            </li>
                        </ul>
                    </div>

                    <a class="button primary-btn my-3" id="resetFilters" href="#">Reset Filters</a>

                </div>

                <div class="col-xl-9 col-lg-8 col-md-7">
                    <!-- Start Filter Bar -->
                    <div class="filter-bar d-flex flex-wrap align-items-center">
                        <div class="sorting mr-auto">

                            <div class="form-check form-check-inline">
                                <span class="form-check-label">Price sort: </span>
                            </div>

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sort" id="sortAsc"
                                       value="asc">
                                <label class="form-check-label" for="sortAsc"><i
                                        class="fas fa-sort-amount-up"></i></label>
                            </div>

                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="sort" id="sortDesc"
                                       value="desc">
                                <label class="form-check-label" for="sortDesc"><i
                                        class="fas fa-sort-amount-down"></i></label>
                            </div>

                        </div>

                        <div>
                            <div class="input-group filter-bar-search">
                                <input type="text" placeholder="Search" id="tbSearch">
                                <div class="input-group-append">
                                    <button type="button"><i class="ti-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Filter Bar -->
                    <!-- Start Best Seller -->
                    <section class="lattest-product-area pb-40 category-list">
                        <div class="row" id="products">
                            @foreach($products as $p)

                                <div class="col-md-6 col-lg-4">
                                    @component("front.components.product", [
                                "p" => $p
                            ])
                                    @endcomponent

                                </div>

                            @endforeach

                        </div>

                        {{$products->links('front.pagination-bootstrap-4')}}

                    </section>
                    <!-- End Best Seller -->
                </div>
            </div>
        </div>
    </section>
    <!-- ================ category section end ================= -->

@endsection
