@extends('layouts.front')
@section('title')
    Orders
@endsection

@section('currentPage')
    Order History
@endsection

@section('main')

    @include('front.fixed.breadcrumbs')

    @include('msg')

    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">

                @if(isset($orders) && count($orders))

                    <div class="table-responsive " style="min-height: 20em;">

                        <table class="table ">
                            <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Number of Different Products</th>
                                <th scope="col">Grand Total</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($orders as $o)
                                <tr>
                                    <td>
                                        <h5>
                                            <a href="{{route('orders.details',$o->id)}}">
                                                {{$o->getDate()}}
                                            </a>
                                        </h5>
                                    </td>
                                    <td><h5>{{count($o->orderDetails)}}</h5></td>
                                    <td><h5>${{$o->total()}}</h5></td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                    </div>

                @else
                    <div style="height: 20em;" class="text-center">
                        <h5 class=" text-warning py-5 ">Your Order History is empty</h5>
                        <a class="gray_btn " href="{{route('products.index')}}">Continue
                            Shopping</a>
                    </div>


                @endif


            </div>


        </div>
    </section>
    <!--================End Cart Area =================-->

@endsection

