@extends('layouts.front')
@section('title')
    Login
@endsection

@section('currentPage')
    Login / Register
@endsection


@section('main')

    @include('front.fixed.breadcrumbs')

    <!--================Login Box Area =================-->
    <section class="login_box_area section-margin">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login_box_img">
                        <div class="hover">
                            <h4>New to our website?</h4>
                            <p>You are going to need an account in order to be able to buy our products.</p>
                            <a class="button button-account" href="{{route('form.register')}}">Create an Account</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login_form_inner">

                        @include('msg')

                        <h3>Log in to enter</h3>
                        <form class="row login_form" action="{{route("login")}}" id="contactForm" name="loginForm"
                              method="POST">
                            @csrf

                            <div class="col-md-12 form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email Address"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'"
                                       value="{{old('email')}}">
                                @error('email')
                                <div class="text-danger">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                                @error('password')
                                <div class="text-danger">
                                    {{$message}}
                                </div>
                                @enderror

                            </div>
                            <div class="col-md-12 form-group">
                                <button type="submit" value="submit" class="button button-login w-100">Log In</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Login Box Area =================-->
@endsection

