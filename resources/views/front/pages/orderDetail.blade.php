@extends('layouts.front')
@section('title')
    Order Details
@endsection

@section('currentPage')
    Order Details
@endsection

@section('main')

    @include('front.fixed.breadcrumbs')


    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($order->orderDetails as $d)

                            @component("front.components.orderItem", ["d" => $d, "editable" => false ])
                            @endcomponent

                        @endforeach
                        <tr>
                            <td colspan="2">
                                Order was placed at: {{$order->getDate()}} CET
                            </td>
                            <td>
                                <h5>Subtotal</h5>
                            </td>
                            <td>
                                <h5>${{$order->total()}}</h5>
                            </td>
                        </tr>

                        <tr class="out_button_area">
                            <td colspan="4">
                                <a class="gray_btn" href="{{route('orders.list')}}">Order History</a>
                            </td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>

@endsection
