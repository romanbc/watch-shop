@extends('layouts.front')

@section('title')
    Home
@endsection

@section('cssVendors')
    @parent

    <link rel="stylesheet" href="{{ asset("vendors/owl-carousel/owl.theme.default.min.css")}}">
    <link rel="stylesheet" href="{{ asset("vendors/owl-carousel/owl.carousel.min.css")}}">
@endsection

@section('jsVendors')
    @parent
    <script src="{{ asset("vendors/owl-carousel/owl.carousel.min.js")}}"></script>
    <script src="{{ asset("js/home.js") }}"></script>
@endsection

@section('main')
    <main class="site-main">
        <!--================ Hero banner start =================-->
        <section class="hero-banner">
            <div class="container">
                <div class="row no-gutters align-items-center pt-60px">
                    <div class="col-5 d-none d-sm-block">
                        <div class="hero-banner__img">
                            <img class="img-fluid" src="{{asset('img/home/hero-banner.png')}}" alt="">
                        </div>
                    </div>
                    <div class="col-sm-7 col-lg-6 offset-lg-1 pl-4 pl-md-5 pl-lg-0">
                        <div class="hero-banner__content">
                            <h4>Shop is fun</h4>
                            <h1>Browse Our Premium Products</h1>
                            <p>At Watch Shop, we can guarantee you will find more watch brands than anywhere else. From the biggest designer brands, to the best of luxury and some hidden gems, along with outstanding service and next day delivery, Watch Shop is the hottest place to buy your next watch. </p>
                            <a class="button button-hero" href="{{route('products.index')}}">Browse Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ================ Best Selling item  carousel ================= -->
        <section class="section-margin calc-60px">
            <div class="container">
                <div class="section-intro pb-60px">
                    <p>Buy a watch now</p>
                    <h2>Best <span class="section-intro__style">Deals</span></h2>
                </div>

                <div class="owl-carousel owl-theme" id="bestSellerCarousel">

                    @foreach($products as $p)
                        @component("front.components.product", [
                            "p" => $p
                        ])
                        @endcomponent
                    @endforeach

                </div>
            </div>
        </section>
    </main>
@endsection

