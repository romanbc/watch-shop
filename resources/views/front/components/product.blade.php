<div class="card text-center card-product">
    <div class="card-product__img">

        <a href="{{route('products.show',$p->id)}}">
            <img class="img-fluid" src="{{asset('img/products/' . $p->image)}}" alt="image_of_{{$p->name}}">
        </a>
    </div>

    <div class="card-body">
        <p >{{$p->brand->name}}</p>
        <h4 class="card-product__title">
            <a href="{{route('products.show',$p->id)}}">{{$p->name}}</a>
        </h4>

        <div class="card-product__price">
            <div class="discounted-price">
                @if($p->discount > 0)
                    <del>
                        ${{round($p->price,2)}}
                    </del>
                    <ins>
                        ${{ round(($p->price * (100 - $p->discount) / 100), 2) }}
                    </ins>
                    <p class="text-primary">Save {{round($p->discount)}}% today</p>
                @else
                    <ins>
                        ${{round($p->price,2)}}
                    </ins>
                @endif
            </div>
        </div>

    </div>
</div>

