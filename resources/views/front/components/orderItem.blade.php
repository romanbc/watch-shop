<tr>

    @if($editable)
    <td>
        <form method="POST" action="{{route('cart.delete')}}">
            @csrf

            <input type="hidden" name="product_id" value="{{$d->product_id}}"/>

            <button type="submit"
                    class="btn btn-sm btn-outline-danger remove ml-auto">
                <i class="fa fa-trash"></i>
            </button>
        </form>

    </td>
    @endif

    <td>
        <div class="media">
            <div class="d-flex">
                <img src="{{asset('img/products/'. $d->product->image)}}" alt=""
                     width="60em">
            </div>
            <div class="media-body">
                <p>
                    <a href="{{route('products.show',$d->product->id)}}">{{$d->product->name}}</a>
                </p>
            </div>
        </div>
    </td>

    <td>
        <h5>${{round($d->product_price,2)}}</h5>
    </td>
    <td>
        <h5>{{$d->quantity}}</h5>
    </td>
    <td>
        <h5>${{$d->total()}}</h5>
    </td>

</tr>
