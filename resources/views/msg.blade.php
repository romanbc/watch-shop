@if(session()->has('msgError'))
    <div class="container mt-4">
        <div class="alert alert-danger" role="alert">
            {{ session('msgError')}}
        </div>
    </div>
@endif

@if(session()->has('msgSuccess'))
    <div class="container mt-4">
        <div class="alert alert-success" role="alert">
            {{ session('msgSuccess')}}
        </div>
    </div>
@endif
