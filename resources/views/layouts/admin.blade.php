<!DOCTYPE html>
<html lang="en">
@include('admin.fixed.head')
<body class="animsition">
<div class="page-wrapper">
    @include('admin.fixed.header')
    @include('admin.fixed.side')
    @include('admin.fixed.content')
</div>
@include('admin.fixed.scripts')
</body>
</html>
