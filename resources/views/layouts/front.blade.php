<!DOCTYPE html>
<html lang="en">
@include('front.fixed.head')
<body>
@include('front.fixed.header')
@yield('main')
@include('front.fixed.footer')
@include('front.fixed.scripts')
</body>
</html>
