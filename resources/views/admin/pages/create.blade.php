@extends('layouts.admin')

@section('title')
    {{ucfirst($table)}}
@endsection

@section('main')
    <div class="card">
        <div class="card-header">New {{$what}}</div>
        @include('admin.pages.form', ["action" => "store"])
    </div>
@endsection
