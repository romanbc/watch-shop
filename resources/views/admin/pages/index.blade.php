@extends('layouts.admin')
@section('title')
    {{ucfirst($table)}}
@endsection
@section('main')
    @if($editable)
        <a href="{{route("admin.${table}.create")}}" class="btn btn-primary btn-md active m-b-10" role="button"
           aria-pressed="true">New {{$what}}</a>
    @endif
    @if($items->isEmpty())
        <div class="alert alert-warning" role="alert">
            No records found @if($editable), add a new one @endif
        </div>
    @else
        <div class="table-responsive table--no-card m-b-30">
            <table class="table table-borderless table-striped table-earning">
                <thead>
                <tr>
                    @if($editable)
                        <th>Action</th>
                    @endif
                    @foreach($columns as $column)
                        <th>{{array_slice(explode('->', $column), -2, 1)[0]}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>

                @foreach($items as $item)
                    <tr>
                        @if($editable)
                            <td>
                                <div class="table-data-feature">
                                    <a class="item" data-toggle="tooltip" data-placement="top"
                                       title="Edit"
                                       href="{{route("admin.${table}.edit",$item->id)}}">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>
                                    <form action="{{route("admin.${table}.destroy",$item->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="item" data-toggle="tooltip" data-placement="top"
                                                title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        @endif

                        @foreach($columns as $column)
                            <td>
                                @if( !strpos($column,'->'))
                                    {{$item->$column}}
                                @else
                                    {{array_reduce(explode('->', $column), function ($o, $p) { return !empty($o) ? $o->$p : 'unknown' ; }, $item)}}
                                @endif
                            </td>
                        @endforeach
                    </tr>
                @endforeach

                </tbody>
            </table>
            @endif
        </div>

        {{$items->links()}}

@endsection
