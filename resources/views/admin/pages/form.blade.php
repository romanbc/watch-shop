<div class="card-body">

    <form
        action="@if($action == "update") {{ route("admin.${table}.${action}", $item['id']) }} @else {{ route("admin.${table}.${action}") }} @endif"
        method="POST" novalidate="novalidate" id="form"
        @if($table === 'products') enctype="multipart/form-data" @endif >

        @csrf
        @if($action == "update")
            @method('PUT')
        @endif

        @foreach($fields as $f)
            <div class="form-group">
                @if(in_array($f['type'],['text','number','password']))
                    <label for="field-{{$f['column']}}"
                           class="control-label mb-1">{{$f['text']}}</label>
                    <input id="field-{{$f['column']}}" name="{{$f['column']}}"
                           type="{{$f['type']}}" class="form-control"
                           aria-required="true" aria-invalid="false"
                           @if($f['type']=='number') min="0"
                           @endif value="{{ $item[$f['column']] ?? old($f['column']) }}">

                @elseif($f['type'] === 'textarea')
                    <label for="field-{{$f['column']}}"
                           class="control-label mb-1">{{$f['text']}}</label>
                    <textarea name="{{$f['column']}}" id="field-{{$f['column']}}"
                              rows="6"
                              class="form-control">{{ $item[$f['column']] ?? old($f['column']) }}</textarea>

                @elseif($f['type'] === 'select')
                    <label for="field-{{$f['column']}}"
                           class="control-label mb-1">{{$f['text']}}</label>
                    <select name="{{$f['column']}}" id="field-{{$f['column']}}"
                            class="form-control-lg form-control">

                        @foreach($f['pairs'] as $p)
                            <option value="{{$p['id']}}"
                                    @if(( $item[$f['column']] ?? old($f['column']) ) == $p['id'])
                                    selected
                                @endif
                            >{{$p['name']}}</option>
                        @endforeach

                    </select>


                @elseif($f['type'] === 'file')
                    <label for="field-{{$f['column']}}"
                           class="control-label mb-1">{{$f['text']}}</label>

                    <input type="file" id="field-{{$f['column']}}" name="{{$f['column']}}"
                           class="form-control-file">

                @elseif($f['type'] === 'radio')
                    <div class="form-group">
                        {{$f['text']}}
                    </div>
                    <div class="form-group">
                        <div class="form-check-inline form-check">
                            @foreach($f['pairs'] as $p)

                                <label for="field-{{$f['column'] . '-' . $p['value']}}"
                                       class="form-check-label mx-1">
                                    <input type="radio"
                                           id="field-{{$f['column'] . '-' . $p['value']}}"
                                           name="{{$f['column']}}"
                                           value="{{$p['value']}}"
                                           class="form-check-input"
                                           @if( ( $item[$f['column']] ?? old($f['column']) ) == $p['value'] ) checked
                                        @endif
                                    >
                                    {{$p['text']}}
                                </label>
                            @endforeach
                        </div>
                    </div>

                @endif

                @error($f['column'])
                <div class="text-danger">
                    {{$message}}
                </div>
                @enderror
            </div>
        @endforeach
    </form>
</div>

<div class="card-footer">
    <button type="submit" form="form" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Submit
    </button>
    <button type="reset" form="form" class="btn btn-danger btn-sm">
        <i class="fa fa-ban"></i> Reset
    </button>
</div>
