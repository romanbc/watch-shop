@extends('layouts.admin')

@section('title')
    {{ucfirst($table)}}
@endsection

@section('main')
    <div class="card">
        <div class="card-header">Edit {{$what}}</div>
        @include('admin.pages.form', ["action" => "update"])
    </div>
@endsection
