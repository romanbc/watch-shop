<div class="page-container">
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        @include('msg')
                        <h3 class="title-5 m-b-25">{{$title}}</h3>
                        @yield('main')
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
