<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="{{asset('adminassets/images/icon/logo.png')}}" alt="Cool Admin"/>
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                @foreach($links['icons'] as $link)
                    <li>
                        <a href="{{route($link['route'])}}">
                            <i class="fas fa-{{$link['icon']}}"></i>{{$link['name']}}</a>
                    </li>
                @endforeach
                <li class=" has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-chart-bar"></i>Logs</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        @foreach($links['logs'] as $link)
                            <li>
                                <a href="{{route($link['route'])}}">{{$link['name']}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class=" has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Orders</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        @foreach($links['orders'] as $link)
                            <li>
                                <a href="{{route($link['route'])}}">{{$link['name']}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class=" has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-table"></i>Tables</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        @foreach($links['tables'] as $link)
                            <li>
                                <a href="{{route($link['route'])}}">{{$link['name']}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="{{route('logout')}}">
                        <i class="zmdi zmdi-power"></i>Logout
                    </a>
                </li>

            </ul>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR-->
