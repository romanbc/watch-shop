<!-- HEADER MOBILE-->
<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="{{route('home')}}">
                    <img src="{{asset('adminassets/images/icon/logo.png')}}" alt="CoolAdmin"/>
                </a>
                <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                @foreach($links['icons'] as $link)
                    <li>
                        <a href="{{route($link['route'])}}">
                            <i class="fas fa-{{$link['icon']}}">
                            </i>{{$link['name']}}
                        </a>
                    </li>
                @endforeach
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-chart-bar"></i>Logs</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        @foreach($links['logs'] as $link)
                            <li>
                                <a href="{{route($link['route'])}}">{{$link['name']}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Orders</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        @foreach($links['orders'] as $link)
                            <li>
                                <a href="{{route($link['route'])}}">{{$link['name']}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-table"></i>Tables
                    </a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        @foreach($links['tables'] as $link)
                            <li>
                                <a href="{{route($link['route'])}}">{{$link['name']}}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li>
                    <a href="{{route('logout')}}">
                        <i class="zmdi zmdi-power"></i>Logout
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE-->
