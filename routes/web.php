<?php


use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\LogActivityController;
use App\Http\Controllers\Admin\LogVisitController;
use App\Http\Controllers\Admin\MaterialController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\OrderDetailController;
use App\Http\Controllers\Admin\OrderHeaderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\ProductController as FrontProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'home'])->name('home');
Route::get('/author', [FrontController::class, 'author'])->name('author');

Route::get('/login', [FrontController::class, 'login'])->name('form.login');
Route::get('/register', [FrontController::class, 'register'])->name('form.register');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::post('/register', [AuthController::class, 'register'])->name('register');
Route::middleware('isLoggedIn')->group(function () {
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
});

Route::prefix('admin')->middleware('isAdmin')->name('admin.')->group(function () {

    Route::get('/', [UserController::class, 'index'])->name('index');
    Route::get('visits', [LogVisitController::class, 'index'])->name('visits.index');
    Route::get('activities', [LogActivityController::class, 'index'])->name('activities.index');
    Route::get('orders', [OrderHeaderController::class, 'index'])->name('orders.index');
    Route::get('details', [OrderDetailController::class, 'index'])->name('details.index');

    Route::resource('menus', MenuController::class)->except(['show']);
    Route::resource('materials', MaterialController::class)->except(['show']);
    Route::resource('brands', BrandController::class)->except(['show']);
    Route::resource('users', UserController::class)->except(['show']);
    Route::resource('products', ProductController::class)->except(['show']);

});

Route::get('/products', [FrontProductController::class, 'index'])->name('products.index');

Route::get('/products/{product}', [FrontProductController::class, 'show'])->name('products.show')->whereNumber('product');

Route::post('/products/filter', [FrontProductController::class, 'getFiltered'])->name('products.getFiltered');

Route::middleware('isCustomer')->group(function () {

    Route::get('/cart',[CartController::class, 'showUnProcessedOrder'])->name('cart.index');

    Route::post('/cart',[CartController::class, 'add'])->name('cart.add');
    Route::post('/cart/delete',[CartController::class, 'delete'])->name('cart.delete');

    Route::get('/orders',[CartController::class, 'showProcessedOrders'])->name('orders.list');
    Route::get('/orders/{order}',[CartController::class, 'showOneProcessedOrder'])->name('orders.details')->whereNumber('order');

});

