$(function () {
    "use strict";


    //------- fixed navbar --------//
    $(window).scroll(function () {
        var sticky = $('.header_area'),
            scroll = $(window).scrollTop();

        if (scroll >= 100) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });


    // $('select').niceSelect();

});




function eventScroll() {
    $('html, body').scrollTop() > 180 ? $('#toTop').css('display', 'block') : $('#toTop').css('display', 'none');
}

function toTop() {
    $("html, body").animate({
        scrollTop: 0
    }, 400);
}
$(window).on('scroll', eventScroll);
$('#toTop').on('click', toTop);



