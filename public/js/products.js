const token = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': token
    }
});

$(document).ready(function () {

    $(".page-link").click(loadMoreProducts);

    $("input[name='brands']").click(sortAndFilter);
    $("input[name='materials']").click(sortAndFilter);
    $("input[name='gender']").click(sortAndFilter);
    $("input[name='sort']").click(sortAndFilter);

    $("#tbSearch").keyup(sortAndFilter);

    $("#resetFilters").click(resetFilters);
});

let brands = [];
let materials = [];
let gender = null;
let searchString = "";
let sort = "";

function resetFilters(e) {
    e.preventDefault();

    $("input[name='gender']").prop('checked', false);
    $("input[name='brands']").prop('checked', false);
    $("input[name='materials']").prop('checked', false);
    $("input[name='sort']").prop('checked', false);
    $("#tbSearch").val('');

    sortAndFilter();
}


function getFilters() {

    brands = [];
    materials = [];
    gender = null;
    searchString = "";
    sort = "";

    $.each($("input[name='brands']:checked"), function () {
        brands.push($(this).val());
    });
    $.each($("input[name='materials']:checked"), function () {
        materials.push($(this).val());
    });

    gender = $("input[name='gender']:checked").val();
    sort = $("input[name='sort']:checked").val();

    searchString = $("#tbSearch").val().trim().toLowerCase();


}

function loadMoreProducts(e) {

    e.preventDefault();

    getFilters();

    getProducts($(this).data("page"), gender, brands, materials, searchString, sort);


}

function sortAndFilter() {


    getFilters();

    getProducts(1, gender, brands, materials, searchString, sort);


}


function getProducts(page, gender, brands, materials, search, sortValue) {
    const caller = arguments.callee.caller.name;
    $.ajax({
        url: baseUrl + "/products/filter",
        // method: "get",
        method: "POST",
        data: {
            page,
            gender,
            brands,
            materials,
            search,
            sortValue
        },
        dataType: "json",
        success: function (response) {

            showProducts(response.data);



            if (caller == 'sortAndFilter') {
                changePagination(response.last_page, response.current_page);
            }
            if (caller == 'loadMoreProducts') {
                changeActivePageLink(response.current_page);
            }
        },
        error: function (xhr) {
            console.error(xhr);
        }
    });
}

function changePagination(totalLinks, currentPage) {
    let html = "";
    for (let i = 1; i <= totalLinks; i++) {
        if (i != currentPage) {
            html += `<li class="page-item"><a class="page-link" id="link${i}" data-page="${i}" href="#">${i}</a></li>`;
        } else {
            html += `<li class="page-item active"><a class="page-link" id = "link${i}" data-page="${i}" href="#">${i}</a></li>`;
        }
    }
    $(".pagination").html(html);
    $(".page-link").click(loadMoreProducts);
}

function changeActivePageLink(currentPage) {
    $('.page-item').removeClass('active');
    $('#link' + currentPage).parent().addClass('active');
}

function showProducts(products) {

    console.log(products);

    let html = "";

    for (let p of products) {

        let priceOutput = "";

        if(p.discount > 0) {

            let newPrice = Math.round((p.price * (100 - p.discount) / 100));

            priceOutput = `
            <del>
                $${Math.round(p.price)}
            </del>
            <ins>
                $${newPrice}
            </ins>
             <p class="text-primary">Save ${Math.round(p.discount)}% today</p>
            `;
        } else {
            priceOutput = `
            <ins>
                $${Math.round(p.price)}
            </ins>
            `;
        }

        html += `
<div class="col-md-6 col-lg-4">
<div class="card text-center card-product">
    <div class="card-product__img">
        <a href="products/${p.id}">
            <img class="img-fluid" src="${publicFolder}/img/products/${p.image}" alt="image_of_${p.name}">
        </a>
        </div>
    <div class="card-body">
        <p>${p.brand.name}</p>
        <h4 class="card-product__title">
            <a href="products/${p.id}">${p.name}</a>
        </h4>

        <div class="card-product__price">
            <div class="discounted-price">
                    ${priceOutput}
            </div>
        </div>


    </div>
</div>
</div>


            `;
    }

    $("#products").html(html);

}

